import * as actionTypes from "./actionTypes";
import axios from '../axios/axios';

const fetchCategoriesSuccess = data => {
  return {
    type: actionTypes.FETCH_CATEGORIES,
    data: data
  }
}

export const fetchCategories = () => {
  return (dispatch) => {
    axios.get(`/rest/V1/api/homemenucategories/v1.0.1?device_type=mob`)
      .then(response => {
        dispatch(fetchCategoriesSuccess(response.data));
      })
      .catch(error => {
        console.error('Fetch categories Error : ', error);
      })
  }
}

const fetchProductsSuccess = productData => {
  return {
    type: actionTypes.FETCH_PRODUCTS,
    productData: productData
  }
}
export const fetchProducts = id => {
  const endpoint = `/rest/V1/api/catalog/v1.0.1?category_id=${id}`;
  return (dispatch) => {
    axios.get(endpoint)
      .then(response => {
        dispatch(fetchProductsSuccess(response.data));
      })
      .catch(error => {
        console.error('Fetch products Error : ', error);
      })
  }
}