import React from "react";
import ProductListCard from '../ProductListCard';
import { connect } from 'react-redux';
import './ProductListing.css';

const ProductListing = props => {
  const productList = props.products;
  const itemsToShow = props.itemsToShow;
  return (
    <div className="product-list">
      {productList && productList.length !== 0 &&
        productList.slice(0, itemsToShow).map((item, index) => <ProductListCard
          key={item.id}
          image_urls={item.image_urls.x120}
          name={item.name}
          final_price={item.final_price}
          price={item.price}
          rating={item.rating}
          is_in_stock={item.is_in_stock}
          weight={item.weight}
          weight_unit={item.weight_unit}
        />)}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    products: state.products
  };
};

export default connect(
  mapStateToProps,
  null
)(ProductListing);