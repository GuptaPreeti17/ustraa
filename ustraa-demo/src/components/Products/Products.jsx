import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import ProductListing from '../ProductListing';
import ProductControl from '../ProductControl';
import * as actionCreators from "../../Actions/actions";
import './Products.css';
function TabPanel(props) {
	const { children, value, index, tabid } = props;
	return (
		<div
			role="tabpanel"
			hidden={value !== tabid}
			id={`scrollable-force-tabpanel-${index}`}
			aria-labelledby={`scrollable-force-tab-${index}`}
			style={{ margin: "10px" }}
			tabid={tabid}
		>
			{children}
		</div>
	);
}

TabPanel.propTypes = {
	children: PropTypes.node,
	index: PropTypes.any.isRequired,
	value: PropTypes.any.isRequired,
};

function a11yProps(index) {
	return {
		id: `scrollable-force-tab-${index}`,
		'aria-controls': `scrollable-force-tabpanel-${index}`,
	};
}

const Products = props => {

	const { categories, products } = props;
	const [value, setValue] = useState('Sale');
	const [currentTabId, setCurrentTabId] = useState(185);
	const [viewButtonText, setViewButtonText] = useState('[+] View more');
	const [isToggle, setIsToggle] = useState(false);
	const [itemsToShow, setItemsToShow] = useState(3);

	const handleCatChange = (event, newValue) => {
		const changedValue = event.target.value;
		setValue(changedValue);
		const foundCategoryId = categories.find(element => element.category_name === changedValue).category_id;
		setCurrentTabId(foundCategoryId);
	};

	const handleChange = (event, newValue) => {
		setValue(newValue);
	};

	const handleTabClick = (event, id) => {
		setCurrentTabId(id);
	}

	const handleViewButtonClick = event => {
		if (!isToggle) {
			setIsToggle(true);
			setItemsToShow(categories.length);
			setViewButtonText('[-] View less');
		} else {
			setIsToggle(false);
			setItemsToShow(3);
			setViewButtonText('[+] View more');
		}
	}

	useEffect(() => {
		props.fetchProducts(currentTabId);
	}, [currentTabId]);

	return (
		<div className="products">
			<div className="product-root">
				<AppBar position="static" color="default">
					<Tabs
						value={value}
						onChange={handleChange}
						variant="scrollable"
						indicatorColor="primary"
						textColor="primary"
						aria-label="scrollable tabs"
					>
						{categories && categories.length !== 0 &&
							categories.map((item, index) => <Tab
								onClick={(e) => handleTabClick(e, item.category_id)}
								tabfor={item.category_name}
								key={item.category_id}
								className="custom-tab"
								value={item.category_name}
								label={item.category_name}
								name={item.category_name}
								style={{
									backgroundImage: `url(${item.category_image})`,
									backgroundPosition: 'center',
									backgroundSize: 'cover',
									backgroundRepeat: 'no-repeat'
								}}
								{...a11yProps(index)}
							/>)
						}
						<Tab
							className="custom-tab view-all-tab"
							label="View All"
							id="scrollable-force-tab-last"
							aria-controls="scrollable-force-tabpanel-last" />
					</Tabs>
				</AppBar>
				{categories && categories.length !== 0 &&
					categories.map((item, index) => <TabPanel
						key={item.category_id}
						tabid={item.category_name}
						value={value}
						index={index}
					>
						<ProductListing products={products} itemsToShow={itemsToShow} />
					</TabPanel>
					)}

			</div>
			<ProductControl
				catValue={value}
				handleCatChange={handleCatChange}
				viewButtonText={viewButtonText}
				handleViewButtonClick={handleViewButtonClick}
			/>
		</div>
	);

}

const mapStateToProps = state => {
	return {
		categories: state.categories,
		products: state.products
	};
};

const mapDispatchToProps = dispatch => {
	return {
		fetchProducts: type => dispatch(actionCreators.fetchProducts(type))
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Products);