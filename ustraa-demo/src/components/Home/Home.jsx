import React, { Component } from "react";
import { Grid } from "@material-ui/core";
import Products from '../Products';
import { connect } from "react-redux";
import * as actionCreators from "../../Actions/actions";
import './Home.css';

class Home extends Component {
  componentDidMount() {
    this.props.fetchCategories();
  }
  render() {
    return (
      <Grid container>
        <div className="main-wrapper">
          <div className="home-page-title-holder overflow-x-hidden">
            <span className="home-page-title">Our Products</span>
          </div>
          <Products />
        </div>
      </Grid>
    );
  }
};

const mapDispatchToProps = dispatch => {
  return {
    fetchCategories: () => dispatch(actionCreators.fetchCategories())
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Home);