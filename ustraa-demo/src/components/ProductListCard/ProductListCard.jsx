import React from "react";
import Button from '@material-ui/core/Button';
import './ProductListCard.css'

const ProductListCard = ({ image_urls,
	name,
	final_price,
	price,
	rating,
	is_in_stock,
	weight,
	weight_unit }) => {

	return (
		<div className="product-wrapper">
			<div className="imageContainer">
				<img
					className="product-card-image"
					src={image_urls}
					data-src={image_urls}
					alt={name} />
			</div>
			<div className="infoContainer">
				<div className="product-info">
					<div className="ProductNameWrapper">
						<p className="ProductName">{name}</p>
					</div>
					{weight !== 0 && <p className="Weight">({weight} {weight_unit})</p>}
					<div className="PriceBox">
						{final_price && <p className="SpecialPrice">₹ {final_price}</p>}
						{price && <p className="Price">₹ {price}</p>}
					</div>
					<div className="cartButtonWrapper">
						{is_in_stock ?
							<Button variant="contained" className="AddToCartButton common-btn">
								add to cart
				      </Button> :
							<Button variant="contained" disabled className="OutOfStockButton common-btn">
								out of stock
								</Button>
						}
					</div>

				</div>
				<div className="RatingsContainer">
					{rating &&
						<Button variant="outlined" className="ProductCardSimple__RatingsIcon">
							<span className="rating-text">{rating}</span>
							<svg viewBox="0 0 24 24" style={{ display: "inline-block", color: "rgba(0, 0, 0, 0.87)", fill: "rgb(117, 117, 117)", height: "14px", width: "14px", userSelect: "none", transition: "all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms" }}>
								<path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path>
							</svg>
						</Button>}
				</div>
			</div>
		</div>
	);
};

export default ProductListCard;
