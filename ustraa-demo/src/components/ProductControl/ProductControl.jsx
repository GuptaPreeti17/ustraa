import React from "react";
import { connect } from "react-redux";
import './ProductControl.css';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const categoryNames = [
	"Sale",
	"Essentials",
	"Gifts For Men",
	"COLOGNE",
	"Trimmer",
	"Face & Body",
	"Beard ",
	"Hair",
	"Skin",
];

const ProductControl = ({ catValue, handleCatChange, viewButtonText, handleViewButtonClick }) => {
	const [open, setOpen] = React.useState(false);

	const handleClose = () => {
		setOpen(false);
	};

	const handleOpen = () => {
		setOpen(true);
	};

	return (
		<div className="ProductControl">
			<div className="ExpandWrapper">
				<div className="control-left" onClick={handleOpen}>
					<div className="control-left-ss1"> </div>
					<div className="control-left-ss2" id="undefined--undefined-58174" >
						<div className="control-left-ss3">
							<div className="control-left-ss4"></div>
							<div className="control-left-ss5" >{catValue}</div>
							<button className="control-left-ss6" tabIndex="0" type="button">
								<>
									<svg viewBox="0 0 24 24" style={{ display: "inline-block", color: "rgba(0, 0, 0, 0.87)", fill: "inherit", height: "24px", width: "24px", userSelect: "none", transition: "all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms" }}>
										<path d="M7 10l5 5 5-5z"></path>
									</svg>
								</>
							</button>
							<div className="control-left-ss7"></div>
							<div className="control-left-ss8"></div>
						</div>
						<div style={{ display: "none" }}></div>
					</div>
					<>
						<hr className="control-left-ss9" aria-hidden="true" />
						<hr className="control-left-ss10" aria-hidden="true" />
					</>
				</div>
				<p className="SelectBoxText"> Showing for </p>
				<p className="SelectBoxIcon">change</p>
				<button
					className="ViewMoreButton"
					onClick={(e) => handleViewButtonClick(e)}
				>
					{viewButtonText}
				</button>
			</div>
			<div className="Select-Category-wrapper">
				{open && <Select
					labelId="demo-controlled-open-select-label"
					id="demo-controlled-open-select"
					open={open}
					onClose={handleClose}
					onOpen={handleOpen}
					value={catValue}
					onChange={(e) => handleCatChange(e)}
				>
					{categoryNames.map(item => <MenuItem key={item} value={item}>{item}</MenuItem>)}
				</Select>}
			</div>
		</div>
	);
};

const mapStateToProps = state => {
	return {
		categories: state.categories,
	};
};

export default connect(
	mapStateToProps,
	null
)(ProductControl);
