import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import logo from '../../ustraLogo.png';
import './Navbar.css';

const Navbar = () => {

  return (
    <header className="grow navbar-header">
      <Toolbar className="header-toolbar">
        <div className="menu-left">
          <IconButton
            edge="start"
            className="menuButton"
            color="inherit"
            aria-label="open drawer"
          >
            <MenuIcon />
          </IconButton>
          <div className="ustraa-logo">
            <img className="icon-ustraa-logo" src={logo} alt="Ustraa Logo" />
          </div>
        </div>
        <div className="head-icons-right">
          <div className="grow" />
          <div className="sectionDesktop">
            <IconButton aria-label="search icon" color="inherit">
              <SearchIcon />
            </IconButton>
            <IconButton aria-label="cart items" color="inherit">
              <Badge
                badgeContent={3}
                color="primary"
                anchorOrigin={{ vertical: 'bottom', horizontal: 'left', }}
              >
                <ShoppingCartIcon />
              </Badge>
            </IconButton>
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-haspopup="true"
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </div>
        </div>
      </Toolbar>
    </header>
  );
}

export default Navbar;
