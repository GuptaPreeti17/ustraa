import axios from 'axios';
const SERVER = `https://backend.ustraa.com`;

const axiosCall = axios.create({
  baseURL: SERVER,
  headers: {
    ContentType: 'application/json',
    charset: 'utf-8',
  }
});
export default axiosCall;
