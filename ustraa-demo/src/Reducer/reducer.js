import * as actionTypes from "../Actions/actionTypes";
import { updateObject } from "./utility";

const initialState = {
  categories: [],
  products: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_CATEGORIES:
      return updateObject(state, {
        categories: action.data.category_list,
        products: action.data.product_list.products
      });
    case actionTypes.FETCH_PRODUCTS:
      return updateObject(state, { products: action.productData.products });
    default:
      return state;
  }
};

export default reducer;
