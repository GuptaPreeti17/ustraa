This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Steps to run the project are: 
- clone this project into your system.
- go to this clonned directory
- open terminal ponting to this directory and In the project directory, you can run: 
    npm install
  to install all the dependent packages.
- after this, run 
    npm start

  to start the project. Runs the app in the development mode.<br />
  Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Thanks
Preeti Gupta